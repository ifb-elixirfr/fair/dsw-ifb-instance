
## Présentations



- [présentation à CDGA, Cellule Data Grenoble Alpes, 29 avril 2024](https://docs.google.com/presentation/d/e/2PACX-1vRFS9k5q5okqNVoa6YUqOkCFO2I5t2-ST00_j0J2VoYAr9btX1DLs1kD8wXVMXVxXtXOlm_nRXEXgK_/pub?start=false&loop=false&delayms=3000)

- [présentation à la journée "Cycle de vie des données en biologie", Sophia Antipolis, 4 avril 2024](https://docs.google.com/presentation/d/e/2PACX-1vTG8s_jwgG536ayohk3plKLx4jYmWWTTMoYtNuQbnB80e89VvmPJCkP-UWjrXw1AKQugI2DD3FEMEcl/pub?start=false&loop=false&delayms=3000)

- [présentation de Jean-François Dufayard aux journées des ADAC, 28 novembre 2023](https://docs.google.com/presentation/d/e/2PACX-1vRrDP_P0lhq5BB45lZIdAw6YzyzQ_rUoSMqvZEqrAuUWUkGPd37_Ap1ozJYKGh9kllqzDX6L4caaOup/pub?start=false&loop=false&delayms=3000)

- [école QuaRES Montpellier, 13 septembre 2023](https://docs.google.com/presentation/d/e/2PACX-1vTSX7PcRRCRNtAawndOh9BkdSnaLDN5sIBMjpyR8qrfC1jL2zeRV-RbvSXNd-RG3kGBANLvGabi4w3z/pub?start=false&loop=false&delayms=3000)

- [BInGIDays CSTB Team (Icube), 30 juin 2023](https://docs.google.com/presentation/d/e/2PACX-1vTkuhUz68ddKyXM8IIqbZPlWPiqBFe2hUOwMcBzNKUETSC5SK_17Q1diKiesoWgUasnJyTE4PFHggmc/pub?start=false&loop=false&delayms=3000)

- [ANF OMERO-FAIRly, 14 juin 2023](https://docs.google.com/presentation/d/e/2PACX-1vT9KgclL3U_IiwEOc0U_OEFNcvUYk8cfTFS_WQomekm4JPr85IB5RtZdQnvieIgX_w_ZXfzcxM5eUEe/pub?start=false&loop=false&delayms=3000)

- [DMP-OPIDoR, 30-31 mai 2023](https://docs.google.com/presentation/d/e/2PACX-1vSWAevNZUacejtLMHyKeLhrwyie5ROI-hDcdsu_yKu5-XnEiy1p8U5824rzDdV4SHlna4RJHmwDPcVr/pub?start=false&loop=false&delayms=3000)

- [pleinière IS1, 23 janvier 2023](https://docs.google.com/presentation/d/e/2PACX-1vSmfsi9flJe8beaktYa2bR_nbH7-ONLJXhvOcA_mBD2FE-TguVDzCb1aY6AfqUqCrjQgV9-6X_xwNoE/pub?start=false&loop=false&delayms=3000)

- [présentation de la trame, 2 mars 2023](https://docs.google.com/presentation/d/e/2PACX-1vRe4pi31EgvLGiQ-euIXhCv4nAkzPJbB-YBaPawQquKjuWR3fkxoP6y1yDUCt3XJ3qNXu5pT8DgrrgH/pub?start=false&loop=false&delayms=3000)

- [pleinière IS1, 5 avril 2022](https://docs.google.com/presentation/d/e/2PACX-1vSi8L5lwwWwXCks9D_HYImr6MvJr6hWTJqzMBtMdX6pUlqN3xyOiuc2h5Eat4911U4rtj5sW579AS_X/pub?start=false&loop=false&delayms=3000)
