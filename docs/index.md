## L'instance DSW@IFB


L'instance [DSW@IFB](https://dsw.france-bioinformatique.fr) 
est hébergée par l'Institut
Français de Bioinformatique, 
[IFB](https://www.france-bioinformatique.fr/en/home/).

Cette instance a deux objectifs : la mise à disposition de l'outil [DSW](https://ds-wizard.org/) à 
la communauté,
et permettre aux membres de du GT2@IS1 du projet [MUDIS4LS](https://www.france-bioinformatique.fr/en/news/mudis4ls-the-project-for-shared-digital-spaces-for-life-sciences/) de collaborer sur des trames de PGD.

L'instance est enregistrée dans [bio.tools](https://bio.tools/dsw_at_ifb).

### L'outil DSW

[DSW](https://ds-wizard.org/) est un outil de support au PGD.


Nous n'offrons pas de support spécifique pour l'outil, mais des tutoriels (en cours de construction). Voir aussi 
le [guide](https://guide.ds-wizard.org/en/latest/) de DSW, très complet.

Devriez vous rencontrer des problèmes liés à l'instance, vous pouvez 
les poster sur notre [gitlab](https://gitlab.com/ifb-elixirfr/fair/dsw-ifb-instance/-/issues).

###  Collaboration GT2@IS1

Dans le cadre de l'IS1, 'Intégration et partage FAIR des données d’imagerie et multi-omiques', le GT2 a pour mission d'élaborer une trame de PGD entité 
pour la bioimagerie et les multi-omiques. Le cahier des charges du GT2 peut se
consulter [ici](https://ifb-elixirfr.gitlab.io/fair/dsw-ifb-instance/cahier_des_charges.html).


## License

Apache-2.0

