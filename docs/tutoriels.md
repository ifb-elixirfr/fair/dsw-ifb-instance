
## Tutoriels


- [créer son PGD avec DSW@IFB](https://docs.google.com/presentation/d/e/2PACX-1vS54Ckm-4156SetlEjS_CqeHce8oDGIUMxVmq_tZbJVKbBljqrISUoE4zs1YUnbT362Lufi_2a47sBs/pub?start=false&loop=false&delayms=3000), mai 2024

- [tutoriel de prise en main DSW fait avec MAMMA](https://dsw.france-bioinformatique.fr/gitlab_ci/2024-01-17/video_tuto_mamma_20240117.mp4) (enregistrement), janvier 2024


- [modèles de documents, 1.](https://docs.google.com/presentation/d/e/2PACX-1vQIe5NYl1M0ZE3BByoSnJ7zURs_6ecRgZ5qZpuhQxpQ3uNJbKkcgQ7rHGrkdUofm-k2gDrONLA_W97j/pub?start=false&loop=false&delayms=3000), avril 2024


- [modèles de documents, 2.](https://docs.google.com/presentation/d/e/2PACX-1vS8HNPRr2EaHNEbvDVIrn3FfBdGeLjOOm4lqTh80ct4vn3lPL1-49yqhfIhAmxFgAM9QyzrINEbJOPv/pub?start=false&loop=false&delayms=3000), avril 2024

- [modification de la trame, modifier une question](https://docs.google.com/presentation/d/e/2PACX-1vSHT4ttmdS2rwnsFiE31RBMDN6e4J-1QXkkgP1TM-wN2XBPsK3ji4xk-WPA_z0KgbI6gFaokJX_qtT3/pub?start=false&loop=false&delayms=3000), avril 2024

## Tutoriels archivés 

Ces tutoriels ne sont plus à jour, mais peuvent tout de même être utiles.

- un [tutoriel](https://docs.google.com/presentation/d/e/2PACX-1vSW786gdltEbDQ21eL6zZH0CNlUzcs33neWy9kWjfbpcLH1xkcl8vvUix8rPuGKpfvZYyCxCj40CZMK/pub?start=false&loop=false&delayms=3000) express de préparation aux ateliers de remplissage, avril 2023

- un [tutoriel](https://gitlab.in2p3.fr/fbi-data/websites/DataManagementPlan/-/blob/main/TutorielCreerVotrePGDdeStructureBioImage.md) de remplissage de la trame, fait pour France BioImaging, 2020-2023

- un [tutoriel](https://docs.google.com/presentation/d/e/2PACX-1vRVE3rk-g89Jhs6-rHwNQVniZxZU64Fgtne9H-2Aaq8X4_m037Ib_SsI2ytIa1BUjUb6qU6IzO4y-_h/pub?start=false&loop=false&delayms=3000) d'édition de la trame, 2023
