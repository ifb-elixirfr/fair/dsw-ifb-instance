

## Enregistrements

### Ateliers de remplissage

- [enregistrement de_l'atelier de retour cytométrie, 7 novembre 2023](https://dsw.france-bioinformatique.fr/gitlab_ci/2023-11-07/video_atelier_cytometrie_20231107.mp4)

- [enregistrement de_l'atelier de remplissage_métabo, 3 octobre 2023](https://dsw.france-bioinformatique.fr/gitlab_ci/2023-10-03/video_atelier_metabo_20231003.mp4)

- [enregistrement de_l'atelier de remplissage_cytométrie, 20 juillet 2023](https://dsw.france-bioinformatique.fr/gitlab_ci/2023-07-20/video_atelier_cytometrie_20230720.mp4)

- [enregistrement du webinaire présentation de la trame à la communauté de cytométrie, 4 juillet 2023](https://dsw.france-bioinformatique.fr/gitlab_ci/2023-07-04/video_presa_trame_cytometrie_20230704.mp4)

### Tutoriels

- [tutoriel de prise en main DSW, fait avec MAMMA, 17 janvier 2024](https://dsw.france-bioinformatique.fr/gitlab_ci/2024-01-17/video_tuto_mamma_20240117.mp4)

### Présentation de la trame du 2 mars 2023

- [enregistrement du webinaire présentation de la trame du 2 mars 2023](https://dsw.france-bioinformatique.fr/gitlab_ci/2023-03-02/audio_presa_trame_20230302.m4a) ; la trame est à retrouver [ici](https://docs.google.com/presentation/d/e/2PACX-1vRe4pi31EgvLGiQ-euIXhCv4nAkzPJbB-YBaPawQquKjuWR3fkxoP6y1yDUCt3XJ3qNXu5pT8DgrrgH/pub?start=false&loop=false&delayms=3000)

