# Pages d'accueil pour dsw.france-bioinformatique.fr

Site web test réalisé avec
[mkdocs](https://www.mkdocs.org/) et [Material for Mkdocs](https://squidfunk.github.io/mkdocs-material/)

Son lien est [DSW@IFB](https://ifb-elixirfr.gitlab.io/fair/dsw-ifb-instance/)
